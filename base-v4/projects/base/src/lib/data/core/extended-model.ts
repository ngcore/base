import { UniqueIdUtil, DateIdUtil, DateTimeUtil, RandomIdUtil } from '@ngcore/core';
import { BaseModel } from './base-model';
import { MediaType } from './media-type';
import { MediaAttachment } from './media-attachment';


/**
 * BaseModel with attachments.
 */
export abstract class ExtendedModel extends BaseModel {

  // static getDocId(userId: string, id: string): string {
  //   return userId + ':' + id;
  // }
  // getDocId(): string {
  //   return this.userId + ':' + this.id;
  // }
  // setDocId(_id: string) {
  //   let idx = -1;
  //   if (_id) {
  //     idx = _id.indexOf(':');
  //   }
  //   if (idx == -1) {
  //     this.userId = null;  // ???
  //     this.id = _id;
  //   } else {
  //     this.userId = _id.substring(0, idx);
  //     this.id = (_id.length > idx + 1) ? _id.substring(idx + 1) : "";  // ???
  //   }
  // }

  // Attachment list. Non-ordered.
  // TBD: How to prevent duplicate entries (or, multiple entries with the same names)??
  // Just use a map???
  // attachments: MediaAttachment[] = [];
  attachments: { [name: string]: MediaAttachment } = {};

  hasAttachment(name: string): boolean {
    return (this.attachments && (name in this.attachments));
  }

  get attachmentSize(): number {
    if(!this.attachments) {
      return 0;
    } else {
      return Object.keys(this.attachments).length;
    }
  }

  // tbd: Just use separate attachment list/map data for each mediaType???
  getMediaAttachments(mediaType: MediaType): { [name: string]: MediaAttachment } {
    let map: { [name: string]: MediaAttachment } = {};
    if(this.attachments) {
      for(let n in this.attachments) {
        let a = this.attachments[n];
        if(a && a.mediaType == mediaType) {
          // a.name = n;
          map[n] = a;
        } 
      }
    }
    return map;
  }

  get audioAttachments(): { [name: string]: MediaAttachment } {
    return this.getMediaAttachments(MediaType.audio);
  }
  get imageAttachments(): { [name: string]: MediaAttachment } {
    return this.getMediaAttachments(MediaType.image);
  }
  get videoAttachments(): { [name: string]: MediaAttachment } {
    return this.getMediaAttachments(MediaType.video);
  }

  hasAudioAttachment(name: string): boolean {
    return (name in this.audioAttachments);
  }
  hasImageAttachment(name: string): boolean {
    return (name in this.imageAttachments);
  }
  hasVideoAttachment(name: string): boolean {
    return (name in this.videoAttachments);
  }

  get audioAttachmentSize(): number {
    return Object.keys(this.audioAttachments).length;
  }
  get imageAttachmentSize(): number {
    return Object.keys(this.imageAttachments).length;
  }
  get videoAttachmentSize(): number {
    return Object.keys(this.videoAttachments).length;
  }

  get hasAudioAttachments(): boolean {
    return (this.audioAttachmentSize > 0);
  }
  get hasImageAttachments(): boolean {
    return (this.imageAttachmentSize > 0);
  }
  get hasVideoAttachments(): boolean {
    return (this.videoAttachmentSize > 0);
  }


  constructor(id: (string | null) = null, userId: (string | null) = null) {
    super(id, userId);
  }


  toString(): string {
    let str = super.toString()
      + '; attachmentSize = ' + this.attachmentSize;
    
    // tbd:
    // display a list of attachment names, at least ???
    
    return str;
  }

  // clone(): ExtendedModel {
  //   let cloned = Object.assign(new ExtendedModel(), this) as ExtendedModel;
  //   return cloned;
  // }
  // static clone(obj: any): ExtendedModel {
  //   let cloned = Object.assign(new ExtendedModel(), obj) as ExtendedModel;
  //   return cloned;
  // }

  // copy(): ExtendedModel {
  //   let obj = this.clone();
  //   // obj.id = RandomIdUtil.id();
  //   obj.id = UniqueIdUtil.id();
  //   obj.resetCreatedTime();
  //   return obj;
  // }
}
