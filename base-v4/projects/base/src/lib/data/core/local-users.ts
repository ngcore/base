/**
 * Defines a set of "special" user IDs.
 */
export namespace LocalUsers {
  // Note 0: These ids are valid locally only. Cannot be used to sync server data.

  // Note 1: "1" is outside the range of valid random ids. Note also that we use a non-zero value.
  // Note 2: Currently, we have to stick to a particular id format, 12 digit hex number string.
  export const NULL_USER_ID: string = "000000000000";
  export const DEFAULT_USER_ID: string = "000000000001";
  // ...
 
}
