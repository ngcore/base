/**
 * Attachment media type.
 * Cruder classification than mime type.
 */
export enum MediaType {
  unknown = 0,
  audio = 1,
  image = 2,
  video = 4,
}
